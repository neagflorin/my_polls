from django.shortcuts import render, get_object_or_404, redirect
from django.views import generic

from .models import User, Question, Choice
from .forms import LogForm, AddQuestionForm, AddChoiceForm, EditQuestionForm, AddChoiceFormset
from django.db import IntegrityError

from datetime import datetime


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'questions_list'

    def get_queryset(self):
        """
           Return all the questions
        """
        return Question.objects.all()


def sign_in(request):
    if request.method == 'POST':
        form = LogForm(request.POST)
        if form.is_valid():
            try:
                user = User(username=form.cleaned_data["username"],
                            password=form.cleaned_data["password"])
                user.save()
                return redirect('polls:user_page', username=user.username)
            except IntegrityError:
                form = LogForm()
                return render(request, "polls/sign_in.html", {
                    "error_msg": "Username already exists",
                    "form": form
                    })

    form = LogForm()
    return render(request, "polls/sign_in.html", {"form": form})


def login(request):
    if request.method == 'POST':
        form = LogForm(request.POST)
        if form.is_valid():
            try:
                user = User.objects.get(username=form.cleaned_data["username"],
                                        password=form.cleaned_data["password"])
                return redirect('polls:user_page', username=user.username)
            except User.DoesNotExist:
                form = LogForm()
                return render(request, "polls/login.html", {
                    "error_msg": "Username or password incorrect",
                    "form": form})

    form = LogForm()
    return render(request, "polls/login.html", {"form": form})


def user_page(request, username):
    user = get_object_or_404(User, username=username)
    questions = Question.objects.filter(user=user)

    return render(request, "polls/user_page.html", {"user": user, "questions": questions})


def detail(request, username, question_id):
    question = get_object_or_404(Question, pk=question_id)
    user = get_object_or_404(User, username=username)

    return render(request, 'polls/detail.html', {"user": user, 'question': question})


def results(request, username, question_id):
    question = get_object_or_404(Question, pk=question_id)
    user = get_object_or_404(User, username=username)

    return render(request, 'polls/results.html', {"user": user, 'question': question})


def vote(request, username, question_id):
    user = get_object_or_404(User, username=username)
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'user': user,
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return redirect('polls:results', username=user.username, question_id=question.id)


def add_question(request, username):
    user = get_object_or_404(User, username=username)
    if request.method == 'POST':
        form = AddQuestionForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data["date"]
            time = form.cleaned_data["time"]
            pub_date = datetime.combine(date, time)
            question = Question(question_text=form.cleaned_data["question_text"],
                                pub_date=pub_date,
                                user=user)
            question.save()
            return redirect('polls:user_page', username=user.username)
        else:
            form = AddQuestionForm()
            return render(request, "polls/add_question.html", {
                "user": user,
                'error_message': "Invalid input",
                'form': form,
            })

    form = AddQuestionForm()
    return render(request, "polls/add_question.html", {"user": user, "form": form})


def add_choices(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    if request.method == 'POST':
        formset = AddChoiceFormset(request.POST)
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data:
                    choice = Choice(choice_text=form.cleaned_data["choice_text"],
                                    votes=form.cleaned_data["votes"],
                                    question=question)
                    choice.save()
            user = question.user
            return redirect('polls:detail', username=user.username, question_id=question_id)

    formset = AddChoiceFormset()
    return render(request, "polls/add_choice.html", {"question": question, "formset": formset})


def edit_question(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    if request.method == 'POST':
        Question.objects.filter(pk=question_id).update(question_text=request.POST["question_text"])
        choices_to_delete_ids = request.POST.getlist("choices")
        for choice_id in choices_to_delete_ids:
            choice = get_object_or_404(Choice, pk=choice_id)
            choice.delete()
        return redirect('polls:detail', username=question.user.username, question_id=question_id)

    choices = set()
    for choice in question.choice_set.all():
        choices.add((choice.id, choice.choice_text))
    form = EditQuestionForm(choices=choices, question_text=question.question_text)
    return render(request, "polls/edit_question.html", {"question": question, "form": form})
