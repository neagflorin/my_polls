from datetime import date, datetime

from django import forms

from .models import Choice


class LogForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'a-zA-Z0-9'}))
    password = forms.CharField(widget=forms.PasswordInput())


class AddQuestionForm(forms.Form):
    question_text = forms.CharField()
    date = forms.DateField(widget=forms.DateInput(attrs={'placeholder': 'YYYY-MM-DD'}), initial=date.today)
    time = forms.TimeField(widget=forms.TimeInput(attrs={'placeholder': 'HH:MM:SS'}), initial=datetime.now)


class AddChoiceForm(forms.Form):
    choice_text = forms.CharField()
    votes = forms.IntegerField(widget=forms.NumberInput(attrs={'min': '0'}), initial=0)

AddChoiceFormset = forms.formset_factory(AddChoiceForm, extra=3)
# AddChoiceFormset = forms.modelformset_factory(model=Choice, fields=['choice_text', 'votes'])


class EditQuestionForm(forms.Form):
    question_text = None
    choices = None

    def __init__(self, choices, question_text):
        forms.Form.__init__(self)
        self.fields["question_text"] = forms.CharField(label="New question text:", initial=question_text)
        self.fields["choices"] = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple,
                                                           choices=choices, label="Select choices to be deleted:")
