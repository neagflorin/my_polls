from django.conf.urls import url

from . import views

app_name = 'polls'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^sign_in/$', views.sign_in, name='sign_in'),
    url(r'^login/$', views.login, name='login'),
    url(r'^user/(?P<username>[a-zA-Z0-9]+)/$', views.user_page, name='user_page'),
    url(r'^user/(?P<username>[a-zA-Z0-9]+)/(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^user/(?P<username>[a-zA-Z0-9]+)/(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    url(r'^user/(?P<username>[a-zA-Z0-9]+)/(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    url(r'^user/(?P<username>[a-zA-Z0-9]+)/add_question/$', views.add_question, name='add_question'),
    url(r'^user/(?P<question_id>[0-9]+)/add_choices/$', views.add_choices, name='add_choices'),
    url(r'^user/(?P<question_id>[0-9]+)/edit_question/$', views.edit_question, name='edit_question'),

]
